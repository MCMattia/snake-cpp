#include <SDL2/SDL.h>
#include <SDL_keycode.h>
#include <algorithm>
#include <iostream>

int main()
{
    static constexpr uint8_t grid_item_size = 20;
    static constexpr uint8_t grid_items = 50;
    static constexpr uint8_t snake_max_length = 100;
    static constexpr SDL_Rect initial_position =
        SDL_Rect{grid_item_size * grid_items / 2, grid_item_size * grid_items / 2, grid_item_size, grid_item_size};
    static constexpr uint16_t initial_delay = 50;

    SDL_Init(SDL_INIT_EVERYTHING);
    auto window = SDL_CreateWindow("Snake", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, grid_item_size * grid_items,
                                   grid_item_size * grid_items, 0);
    auto renderer = SDL_CreateRenderer(window, -1, 0);
    SDL_Event e;

    enum Direction
    {
        UP,
        RIGHT,
        DOWN,
        LEFT,
        NO_DIR
    };

    bool running = true;
    Direction direction = NO_DIR;

    SDL_Rect snake_section[snake_max_length] = {};
    snake_section[0] = initial_position;
    uint8_t snake_length = 10;

    SDL_Rect food = SDL_Rect{rand() % grid_items * grid_item_size, rand() % grid_items * grid_item_size, grid_item_size,
                             grid_item_size};

    bool has_eaten = false;

    uint16_t delay = initial_delay;

    while (running)
    {
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
            {
                running = false;
            }
            else if (e.type == SDL_KEYDOWN)
            {
                if ((e.key.keysym.sym == SDLK_UP) && (direction != DOWN))
                    direction = UP;
                else if ((e.key.keysym.sym == SDLK_RIGHT) && (direction != LEFT))
                    direction = RIGHT;
                else if ((e.key.keysym.sym == SDLK_DOWN) && (direction != UP))
                    direction = DOWN;
                else if ((e.key.keysym.sym == SDLK_LEFT) && (direction != RIGHT))
                    direction = LEFT;
                else if (e.key.keysym.sym == SDLK_ESCAPE)
                    running = false;
            }
        }

        // collision with food
        if ((snake_section[0].x == food.x) && (snake_section[0].y == food.y))
        {
            if (snake_length < snake_max_length)
            {
                snake_section[snake_length] = snake_section[snake_length - 1];
                snake_length++;
                has_eaten = true;
            }
            food = SDL_Rect{rand() % grid_items * grid_item_size, rand() % grid_items * grid_item_size, grid_item_size,
                            grid_item_size};

            if (delay > 10)
                delay -= 10;
        }

        // move
        if ((direction == UP) || (direction == RIGHT) || (direction == DOWN) || (direction == LEFT))
        {
            // update sections from the end to head - 1
            uint8_t i = snake_length - 1;
            if (has_eaten)
            {
                // don't update last section if it has just been added
                i = snake_length - 2;
                has_eaten = false;
            }
            while (i > 0)
            {
                snake_section[i] = snake_section[i - 1];
                i--;
            }

            // update head
            switch (direction)
            {
            case UP:
                snake_section[0].y -= grid_item_size;
                break;

            case RIGHT:
                snake_section[0].x += grid_item_size;
                break;

            case LEFT:
                snake_section[0].x -= grid_item_size;
                break;

            case DOWN:
                snake_section[0].y += grid_item_size;
                break;
            case NO_DIR:
                break;
            }
        }

        // collision with border
        if ((snake_section[0].x < 0) || (snake_section[0].x > grid_item_size * grid_items) ||
            (snake_section[0].y < 0) || (snake_section[0].y > grid_item_size * grid_items))
        {
            snake_section[0] = initial_position;
            snake_length = 1;
            direction = NO_DIR;
            delay = initial_delay;
        }

        // collision with itself
        for (uint8_t i = snake_length - 1; i > 0; i--)
        {
            if ((snake_section[0].x == snake_section[i].x) && (snake_section[0].y == snake_section[i].y))
            {
                snake_section[0] = initial_position;
                snake_length = 1;
                direction = NO_DIR;
                delay = initial_delay;
            }
        }

        // clear window
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        // draw food
        SDL_SetRenderDrawColor(renderer, 120, 120, 120, 120);
        SDL_RenderFillRect(renderer, &food);

        // draw body
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        for (uint8_t i = 0; i < snake_length; i++)
        {
            SDL_RenderFillRect(renderer, &snake_section[i]);
        }

        SDL_RenderPresent(renderer);
        SDL_Delay(delay);
    }
    return 0;
}
